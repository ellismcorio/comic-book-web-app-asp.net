﻿using ComicBookLibraryManagerWebApp.ViewModels;
using ComicBookShared.Data;
using ComicBookShared.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace ComicBookLibraryManagerWebApp.Controllers
{
    /// <summary>
    /// Controller for adding/deleting comic book artists.
    /// </summary>
    public class ComicBookArtistsController : BaseController
    {
        private ComicBookArtistRepository _comicBookArtistRepository = null;
        private ComicBookRepository _comicBookRepository = null;
        private ArtistRepository _artistRepository = null;

        public ComicBookArtistsController()
        {
            _comicBookArtistRepository = new ComicBookArtistRepository(Context);
            _comicBookRepository = new ComicBookRepository(Context);
            _artistRepository = new ArtistRepository(Context);
        }


        public ActionResult Add(int comicBookId)
        {
            // TODO Get the comic book.
            // Include the "Series" navigation property.
            var comicBook = _comicBookRepository.Get(comicBookId);

            if (comicBook == null)
            {
                return HttpNotFound();
            }

            var viewModel = new ComicBookArtistsAddViewModel()
            {
                ComicBook = comicBook
            };

            // TODO Pass the Context class to the view model "Init" method.
            viewModel.Init(Repository, _artistRepository);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Add(ComicBookArtistsAddViewModel viewModel)
        {
            ValidateComicBookArtist(viewModel);

            if (ModelState.IsValid)
            {
                var comicBookArtist = new ComicBookArtist()
                {
                    ComicBookId = viewModel.ComicBookId,
                    ArtistId = viewModel.ArtistId,
                    RoleId = viewModel.RoleId
                };
                _comicBookArtistRepository.Add(comicBookArtist);

                TempData["Message"] = "Your artist was successfully added!";

                return RedirectToAction("Detail", "ComicBooks", new { id = viewModel.ComicBookId });
            }

            viewModel.ComicBook = _comicBookRepository.Get(viewModel.ComicBookId);
            viewModel.Init(Repository, _artistRepository);

            return View(viewModel);
        }

        public ActionResult Delete(int comicBookId, int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var comicBookArtist = _comicBookArtistRepository.Get((int)id);

            if (comicBookArtist == null)
            {
                return HttpNotFound();
            }

            return View(comicBookArtist);
        }

        [HttpPost]
        public ActionResult Delete(int comicBookId, int id)
        {
            // TODO Delete the comic book artist.
            var comicBook = new ComicBook() { Id = id };

            _comicBookArtistRepository.Delete(id);


            //bool saveFailed;
            //do
            //{
            //    saveFailed = false;

            //    try
            //    {
            //        Context.SaveChanges();
            //    }
            //    catch (DbUpdateConcurrencyException ex)
            //    {
            //        saveFailed = true;

            //        // Update the values of the entity that failed to save from the store
            //        ex.Entries.Single().Reload();
            //    }

            //} while (saveFailed);




            TempData["Message"] = "Your artist was successfully deleted!";

            return RedirectToAction("Detail", "ComicBooks", new { id = comicBookId });
        }

        /// <summary>
        /// Validates a comic book artist on the server
        /// before adding a new record.
        /// </summary>
        /// <param name="viewModel">The view model containing the values to validate.</param>
        private void ValidateComicBookArtist(ComicBookArtistsAddViewModel viewModel)
        {
            // If there aren't any "ArtistId" and "RoleId" field validation errors...
            if (_comicBookRepository.ComicBookHasArtistRoleCombination(
                    viewModel.ComicBookId, viewModel.ArtistId, viewModel.RoleId))
            {
                // Then make sure that this artist and role combination 
                // doesn't already exist for this comic book.
                // TODO Call method to check if this artist and role combination
                // already exists for this comic book.
                {
                    ModelState.AddModelError("ArtistId",
                        "This artist and role combination already exists for this comic book.");
                }
            }
        }
    }
}